namespace myflix.Controllers

open System
open System.Collections.Generic
open System.Linq
open System.Threading.Tasks
open Microsoft.AspNetCore.Mvc
open myflix.Models

[<Route("api/[controller]")>]
type MoviesController () =
    inherit Controller()

    [<HttpGet>]
    member this.Get() = 
        seq { yield Movie(Title="Jaws", Director="Steven Spielberg", ReleaseYear=1975)
              yield Movie(Title="Star Wars: Episode IV - A New Hope", Director="George Lucas", ReleaseYear=1977)
              yield Movie(Title="Batman Begins", Director="Christopher Nolan", ReleaseYear=2005)}