namespace myflix.Models

type Movie() = 
    member val Title = "" with get, set
    member val Director = "" with get, set
    member val ReleaseYear = 0 with get, set